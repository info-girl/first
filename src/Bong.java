import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.applet.AudioClip;
import java.applet.*;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Bong extends JApplet implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;
	private Thread thread = null;
	private double x, dx, y, dy;
	private int xSize, ySize;     // ゲーム領域の広さ
	private double paddleXL, paddleYL, paddleXR, paddleYR;  // 棒の位置 XL,YLはLのx座標y座標
	private double paddleSize;   // 棒の長さ
	private String message;
	private String message1;
	private Font font;
	private Font font1;
	private boolean running = true;

	private Image img;     // オフスクリーンイメージ
	private Graphics offg; // オフスクリーン用のグラフィックス
	private int width, height;
	int R = 0;
	int L = 0;
	private AudioClip SE1; 
	
	Panel panel;
	Label label;
	Button button1, button2, button3;
	int level = 1;

	@Override
	public void init() {
		Dimension size = getSize();
		width = size.width; height = size.height;
		xSize = width; ySize = height - 80;
		font = new Font("Monospaced", Font.PLAIN, 12);


		/* パネル */
		panel = new Panel();
		panel.setLayout(null);
		panel.setBackground(Color.white);
		panel.setPreferredSize(new Dimension(481, 400));
		add(panel);

		/* ラベル */
		label = new Label("ﾟ･*:.｡.Please select difficulty level.｡.:*･ﾟ ");
		label.setFont(new Font("Arial",  Font.BOLD | Font.ITALIC, 22));
		label.setForeground(Color.white);
		label.setBackground(Color.pink);
		label.setBounds(30, 100, 420, 50);
		panel.add(label);

		/* ボタン */
		button1 = new Button("easy");
		button1.setBounds(120, 170, 70, 30);
		//button1.setForeground(Color.pink);
		button2 = new Button("nomal");
		button2.setBounds(200, 170, 70, 30);
		button3 = new Button("hard");
		button3.setBounds(280, 170, 70, 30);
		panel.add(button1);
		panel.add(button2);
		panel.add(button3);

		/* ボタンが押されたときの処理 */
		/* button1(level1)がeasy,
		   button2(level2)がnormal, 
		   button3(level3)がhard */
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    paddleSize = 100;
			    dx = 3.2; dy = 2.0;
			    level = 1;
				panel.setVisible(false);
			}
		});
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    paddleSize = 50;
			    dx = 4.0; dy = 3.0;
			    level = 2;
				panel.setVisible(false);
			}
		});
		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    paddleSize = 10;
			    dx = 5.0; dy = 5.0;
			    level = 3;
				panel.setVisible(false);
			}
		});
		

		message = "Game started!";
		message1 = "";
		font = new Font("Monospaced", Font.PLAIN, 12);  // フォントサイズ
		font1 = new Font("Monospaced", Font.BOLD, 30);

		setFocusable(true);
		addKeyListener(this);

		img  = createImage(width, height);
		offg = img.getGraphics();
	}

	private void initialize() {
		// ボールの初期値
		x = 100; y = 100;
		// 変えたらスピード変わった
		// dx = 3.2; dy = 2.0;
		// 棒の初期値
		paddleYL = paddleYR = ySize / 2;
		paddleXL = 30; paddleXR = xSize - 30;
	}
	@Override
	public void paint(Graphics g) {
		// 全体を背景色で塗りつぶす。
		offg.clearRect(0, 0, width, height); 
        
		// 卓球台の枠（色、左線と上線と右線と下線の位置）
		offg.setColor(new Color(255,99,71));
		offg.drawRect(0, 0, xSize - 1, ySize - 1);
		// ボールについて（色、動く範囲と大きさ）
		if ( level == 1 ) {
			offg.setColor(new Color(238,130,238));
			offg.fillOval((int)(x - 3), (int)(y - 3), 20, 20);
		} else if ( level == 2 ) {
			offg.setColor(new Color(238,130,238));
			offg.fillOval((int)(x - 3), (int)(y - 3), 10, 10);
		} else if ( level == 3 ) {
			offg.setColor(new Color(238,130,238));
			offg.fillOval((int)(x - 3), (int)(y - 3), 6, 6);
		}

		offg.setColor(new Color(255,140,0));
		offg.fillRect((int)(paddleXL - 2), (int)(paddleYL - paddleSize / 2), 4, (int)paddleSize);
		offg.setColor(new Color(0,191,255));
		offg.fillRect((int)(paddleXR - 2), (int)(paddleYR - paddleSize / 2), 4, (int)paddleSize);

		offg.setFont(font);
		offg.setColor(Color.GREEN.darker());
		offg.drawString("L: " + L, 5, ySize + 24);
		offg.drawString(R + " :R", 100, ySize + 24); 
		offg.drawString(message, 10, ySize + 12);  // メッセージ出力：メッセージ内容、x座標、y座標		
		offg.setColor(new Color(255,140,0));
		offg.drawString("Left:  Z(Down), W(Up)", 10, ySize + 36);
		offg.setColor(new Color(0,191,255));
		offg.drawString("Right: M(Down), I(Up)", 10, ySize + 48);
		
		offg.setFont(font1);
		offg.setColor(new Color(255,153,204));
		offg.drawString(message1, 150, ySize - 150);

		g.drawImage(img, 0, 0, this);
	}

	public void run() {
		SE1 = Applet.newAudioClip(getClass().getResource("pon.wav"));
		Thread thisThread = Thread.currentThread();
		while (thread == thisThread) {
			initialize();
			requestFocus();
			while (running) {
				x += dx;  y += dy;
				// 左のラケットにあたった
				if (dx < 0 && (x - paddleXL) * (x - dx - paddleXL) <= 0) {
					double rY = y + dy * (paddleXL - x) / dx;
					if ((rY - paddleYL + paddleSize / 2) * (rY - paddleYL - paddleSize / 2) <= 0) {
						x = 2 * paddleXL - x;
						dx *= -1;
						message = "";
						SE1.play();
					}
				}
				// 左の壁にあたった
				if (x < 0) {
					x = -x;
					dx *= -1;
					R++;
				}
				// 右のラケットにあたった
				if (dx > 0 && (x - paddleXR) * (x - dx - paddleXR) <= 0) {
					double rY = y + dy * (paddleXR - x) / dx;
					if ((rY - paddleYR + paddleSize / 2) * (rY - paddleYR - paddleSize / 2) <= 0) {
						x = 2 * paddleXR - x;
						dx *= -1;
						message = "";
						SE1.play();
					}
				}
				// 右の壁にあたった
				if (x > xSize) {
					x = 2 * xSize - x;
					dx *= -1;
					L++;
				}
				// 上の壁にあたった
				if (y < 0) {
					y = -y;
					dy *= -1;
				}
				// 下の壁にあたった
				if (y > ySize) {
					y = 2 * ySize - y;
					dy *= -1;
				}
				if ( L >= 11 && Math.abs(L-R) >= 2 ) {
					stop();
					message1 = "Left Win!!!";
					message = "";
				} else if ( R >= 11 && Math.abs(L-R) >= 2 ){
					stop();
					message1 = "Right Win!!!";
					message = "";
				}
				repaint();
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				}
			}
		}	
	}


	@Override
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	@Override
	public void stop() {
		running = false;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		switch (key) {
		case 'W':  paddleYL -= 10; break;
		case 'Z':  paddleYL += 10; break;
		case 'I':  paddleYR -= 10; break;
		case 'M':  paddleYR += 10; break;
		}
	}

	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			/* タイトルバーに表示する文字列を指定できる */
			JFrame frame = new JFrame("Bong!");       
			/* Bong はクラスの名前にあわせる */
			JApplet applet = new Bong();
			/* アプレット部分のサイズを指定する */
			applet.setPreferredSize(new Dimension(481, 400));
			frame.add(applet);
			frame.pack();
			frame.setVisible(true);
			applet.init();
			applet.start();
			/* ×ボタンを押したときの動作を指定する */
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		});
	}
}
